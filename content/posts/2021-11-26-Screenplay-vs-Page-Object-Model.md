---
title: Screenplay Pattern vs The Page Object Model
date: 2021-11-26 17:00:00
tags:
    - c#
    - "test automation"
categories:
- notes
keywords:
    - c-sharp
    - c#
    - screenplay
    - POM
    - specflow
    - restsharp
    - "boa constrictor"
---

## First up, what this is not
This is not an in-depth review of the ins and outs of the Screenplay pattern in general, or even of the `Boa.Constrictor` framework. Frankly, I believe that kind of insight is only possible when someone personally invests time and "skin in the game" battle-testing something in a real-world production setting. 

If and when that time comes, I will gladly come back and post a more detailed comparison. 

As it stands, I have the [Boa Constrictor tutorial project](https://gitlab.com/jberri/specflow-and-restsharp-using-the-screenplay-pattern) and [my own POM-based implementation](https://gitlab.com/jberri/specflow-and-restsharp-using-the-page-object-model-pattern) of the tutorial to go by for now. With that disclaimer done, let's have a look.

## The Page Object Model
Just how long has this pattern been around? In lieu of any hard evidence, the best I can come up with is **10 years**. I get that number based on the [earliest dated Github POM project](https://github.com/search?o=asc&q=page+object+model&s=updated&type=Repositories) I could find, which seems good enough for a wet-finger-in-the-air estimate. 

10 years later in 2021, the mighty POM is still by far the most dominant pattern when creating automation frameworks, and in all major languages too. But what about going beyond the Page Object Model?

I had this question in mind when I went searching, and the most intriguing result was a recent [c# implementation of the Screenplay pattern](https://q2ebanking.github.io/boa-constrictor/) by developer Andrew Knight. I had heard of it's predecessor, the [Journey Pattern](https://www.slideshare.net/RiverGlide/a-journey-beyond-the-page-object-pattern) some years beforehand but had deemed that too niche and over-engineered. That was then. 

## So why change things if the POM is so much in vogue?
The short answer is "I want something new, or something better". Fair enough, but a better answer might be that having written several ground-up POM frameworks I now have a better appreciation of the POM's strengths and weaknesses, and wanting to improve on those perceived weaknesses is part of what challenges the typical test developer. It does for me.

In Andrew Knight's [article on the same subject](https://q2ebanking.github.io/boa-constrictor/getting-started/page-objects/) he walks through the typical evolution of the common frustrations one runs into when building a POM framework:


1. **Race conditions**: "My tests keep performing actions and assertions on browser elements before the pages have finished loading. I keep getting false negatives and I have to use clumsy hardocded `Thread.Sleep()` statements. Plus, I have to endure overly long development times based on 'let's wait and see what it does this time'. This sucks."

    Clearly unsatisfactory. However, this is apparently solved by ...

2. **Explicit Wait Methods**: "The answer to race conditions! `TimeSpan` wait methods in the WebDriver have saved me from hell, but ... now I am forced to make duplicate calls to the same element and write code that is more difficult to reason about."

It is only at this point that the author introduces the **Page Object Model** pattern as a remedy to fix these problems. However, he introduces a version of the Page Object Model ***without inheritance***. So, **no** base class containing all the typical page interaction methods you would feasibly need. 

I just can't buy that line of reasoning. 

It smacks of unnecessary straw-manning. _Unnecessary_, because I believe that any successor or competitor pattern should be able to demonstrate it's merit by [generously steel-manning](https://en.wikipedia.org/wiki/Straw_man#Steelmanning) the incumbent pattern whilst laying out it's own solution. This is all the more unfortunate as I believe that the Screenplay pattern, and the `Boa.Constrictor` implementation of that pattern does actually have clear merits which we will get into later. 

#### Inheritance

In any company with pockets deep enough to fund a test automation team, whether we're talking Enterprise, SME or solo 10x test-developer in a startup, you are simply not going to find an OOP test automation codebase that does ***not*** utilise base class inheritance for browser interaction methods. Hell, even the [earliest c# Page Object Model project I could find on Github](https://github.com/DmitryRoss/Page-Object-Model-Article/blob/master/TestingProject/TestingProject/Pages/LandingPage.cs) makes use of base class inheritance, and that repo was created in **2012!** In light of this, non-inheritance example projects can simply be dismissed as a straw man argument.

## So what does 'Steel-manning' in this case actually look like?
The fairest comparison between these two patterns really has to begin at a point that typically reflects where we actually are now. In 2021, this means the following:

**A c# project that:**
* Encompasses the real-world needs of a modern test automation framework. This necessarily includes:
    * testing the **UI layer**. Typically that means a browser, and more specifically the browser with the biggest market share, Google's Chrome browser.
    * testing the **service layer**. Typically this means exercising internal API's that are buttressed against internal databases, and/or ***external*** 3rd party API's that come in through the firewall. 

        The internal API's or webservices _may_ require direct access to production domain Models. It _may_ require writing custom extension methods on those Models. It will however ***definitely*** require a programmatic REST/SOAP client that can make requests and parse repsonses across the usual REST/CRUD variety of webservice operations.

* uses Specflow. Personally, I consider Specflow a **required** library.  It is the only real **Behaviour Driven Development** framework in town inside the .NET ecosystem and has utility far beyond executable specifications. Specflow crucially functions as "stakeholder glue", in that it is often the most reliable source of narrative truth for the product accessible to all stakeholders, regardless of technical skill. It is this benefit alone that mandates it's use in my opinion, ***especially*** where automation has not yet been implemented.

* makes full use of **inheritance** in the form of base classes for both:
    1. **browser interaction methods**. e.g. 
        * click
        * enter text
        * select dropdown
        * select radio button
        * scrape table
        * wait, etc.. 

        put all of these methods and functions in a BasePage class that has zero knowledge of user behaviour and Page Objects.

        ***example***: BasePage interaction methods

        ![Base_PageInteractions](../PageBase.png)


    2. **and Step definition methods**:  e.g. serving up actual Pages or common Step Definition methods to step files that inherit it.

        ***example***: the Base_StepDefinition class serving up pages.

        ![Base_StepDefinitions](../StepsBase.png)  


The [POM project I created](https://gitlab.com/jberri/specflow-and-restsharp-using-the-page-object-model-pattern) for this comparison implements these features.

## So what now?
Now we can begin an actual fair comparison. 

#### Race conditions
This is a real thing that has to be mitigated. A combination of network bottlenecks, server-side page rendering and client-side performance can end up stomping over the ideals of snappy page loading.

Selenium's solution comes in the form of it's `SeleniumExtras.WaitHelpers` nuget package. In use it allows you to do the following:

![SeleniumExtras.Wait](../SeleniumExtras.Wait.png)  

Under the hood, the `Until()` method uses a `while` loop to poll for success up to a specfied timeout before continuing or throwing an exception.

The `Boa.Constrictor` framework screenplay solution comes in the form of it's `Actor.WaitsUntil()` method, as for example

![Actor.WaitsUntil](../Actor.WaitsUntil.png)

Under the hood, the `WaitsUntil()` method implements the `WaitForValue()` method that uses a `do while` loop to poll for success up to a specfied timeout before continuing or throwing an exception. 

I like Boa.Constrictor's exit-controlled `do while` approach to waiting, and I _really_ like the `ForAnAdditional(int seconds)` extension method. It allows the tester to fluently create custom waits for unusual or one-off tests.

There are also other approaches to waiting out there. For example, [Microsoft's Playwright framework](https://playwright.dev/dotnet/docs/intro/) solves this problem by mediating every interaction with asynchronous `awaits` e.g.


```
var page = await context.NewPageAsync();

await page.GotoAsync(“https://www.google.com/");
```

All of which is to say that "waiting" is a largely solved issue.

#### Screenplay Design Principles
In summary this is how it works:

* **Actors** initiate Interactions.
* **Abilities** enable Actors to initiate Interactions.
* **Interactions** are procedures that exercise the behaviors under test.
    * **Tasks** execute procedures on the features under test.
    * **Questions** return state about the features under test.
    * **Interactions** may use Locators, Requests, and other Models.


It took some time for me to understand how to think about Tasks and Questions. For instance, 

![TasksAndQuestions](../TasksAndQuestions.png)

Why am I not `AskingFor` the `Actor` to `Navigate.ToUrl`? Well, because "I" am not the Actor and it doesn't makes sense for the `Actor` to be `AskingFor` to `NavigateTo.Url`. That really makes no sense.

A more technical way to understand the difference is:
* `Tasks` are `void` methods that return no values
* `Questions` are functions that return a value

This distinction makes it far easier to understand.


## What is the developer experience like?
The user-centric narrative approach makes a lot of sense to me. My approach to building frameworks and leading teams has always been to prioritise readability and maintainability above everything else. The benefits to having easily reasoned and composable code keep stacking up in the context of wider team adoption.

The trade-off often means lots of low-level complexity to handle every possible interaction and wait condition. A worthwhile trade-off in my opinion.

The `Boa.Constrictor` framework appears to share these same goals, and in terms of fluency and composability, `Boa.Constrictor` is a win. The fact that much of the complexity in `Boa.Constrictor` has been kicked down to abstract classes for dealing with artifacts (the _Dumper_ namespace), logging, utitities, WebDriver wrappers and RestSharp abstractions is exactly the approach I would have used.

_a view of the `Boa.Constrictor` object model with expanded WebDriver namespace:_

![Boa.Constrictor.ObjectModel](../Boa.ObjectModel.Expanded2.png)

The above screenshot demonstrates that it should be able to handle a decent variety of real-life browser interactions out of the box, although I can already see that I might need to add my own methods for html table scraping. 

Additionally, this framework would greatly benefit from better and more extensive documentation. There is practically no documentation at the time of writing, though it should also be noted that the current nuget package is an alpha release.

This notwithstanding, putting `Actors` at the heart of scenarios should in theory allow for the creation of very rich multi Actor scenarios. An obvious example would be financial scenarios where both parties to a transaction can be scripted, but an even better example might be security scenarios involving Alice, Bob, Eve and the [whole supporting cast of cryptographic characters](https://en.wikipedia.org/wiki/Alice_and_Bob) available for this very purpose. I would love to see that.


#### SOLID-compliancy
This appears to be the real win. The gem in putting Actors at the centre of tests is in being able to put runtime boundaries on each Actor's behaviour. For example, giving Alice different abilities to Bob whilst granting Eve all abilities. 

Having the framework [SOLID-compliant](https://q2ebanking.github.io/boa-constrictor/getting-started/screenplay/#the-principles) is what allows this scoping to work, and it feels well-engineered.  

## So what's the verdict?

#### Pro's
Straight off the bat is the fact that I like that this framework exists at all. Having a Screenplay implementation available in the .NET ecosystem can only be a good thing, so hats off to Andrew Knight and his team for putting in the work to create and open-source this framework. Seriously guys, nice work.

Here are the rest of the pros in list form:
* it integrates well with Specflow
* Specflow is _optional_. Honestly, I don't actually recommend this. Specflow has multiple benefits beyond executable scenarios, but UI tests are possible in `Boa.Constrictor` ***without*** Specflow. They also happen to be faster to write.
* in a single package `Boa.Constrictor` does what test developers typically end up doing themselves over time in one way or another, only better. Namely:
    * writing your own collection of WebDriver interaction methods
    * writing custom REST/SOAP clients or extensions to one
    * having a custom solution for logging
    * building tools that allow various artifacts to be collected throughout the test 
    * building a collection of custom helpers
* it puts fair wind behind the push for writing fluent, readable and composable code
* it feels well-engineered
* it is open-sourced and thus forkable and extendable

Arguably the monolithic approach could also be a dependency nightmare. In practice however, all c# test automation projects rely on a Json library, Selenium, Specflow, a Fluent library, and either RestSharp or a custom equivalent. And since `Boa.Constrictor` is forkable, I cannot really hold this against it.

#### Cons
* Documentation is severely lacking. Having a static documentation site with examples and/or a StackOverflow presence would help enormously.
* `Boa.Constrictor` is in alpha release
* It's adoption is tiny


## Concluding remarks
There are still many unknowns. Questions such as scale, team adoption, reliabiility and the presence of bugs remain speculative without real-world use. But perhaps that is what speaks best about `Boa.Constrictor`; the little experience I have gained has actually encouraged me to try it out in anger at some point. And by that I mean in a team setting with multiple environments, multiple builds and CI/CD pipelines. 

That is the point where all such speculations are put to the test, and if I am willing to do that then I believe it says good things about `Boa.Constrictor`s potential.

I look forward to updating my opinion.
